from Preprocessing import preprocessing,get_training_set_files
from DCGAN import DCGAN

if __name__ == '__main__':
    do_preprocessing = False
    batch_size = 32
    epochs= 1000

    # step 1 - Preprocessing
    if do_preprocessing:
        preprocessing(n_mels=64)

    # step 2 - Build GAN
    training_files = get_training_set_files()
    dcgan = DCGAN(training_set = training_files, epochs=epochs, batch_size=batch_size)

    # step 3 - Train GAN
    dcgan.train()


