import os
import matplotlib.pyplot as plt
import librosa
import librosa.display
import numpy as np
import scipy.io.wavfile

highest_db = 80.0
mel_spec_frame_size = 512 # 512 samples in frame = 512/fs milliseconds per frame

def preprocessing(n_mels):
    """
    Performs initial data preprocessing - extracting Mel spectra for all files and padding them appropriately
    :return:
    """
    subfolders, class_labels = get_classes_in_datapath()

    for folder in subfolders:

        if not os.path.exists(os.path.join(folder, 'recon')):
            os.makedirs(os.path.join(folder, 'recon'))

        files_wav = get_wav_files_in_path(datapath=folder)
        for file in files_wav:
            # get melspec
            melspec, fs = get_melspec_from_wav(wavfile=os.path.join(folder, file),
                                               plot=False,
                                               n_mels=n_mels)

            melspec = librosa.power_to_db(melspec, ref=1.0)
            melspec = melspec / highest_db # scale by max dB

            #check we have 64 time samples (and pad)
            if melspec.shape[1] < 64:
                shape = np.shape(melspec)
                padded_array = np.zeros((shape[0],64))-1
                padded_array[0:shape[0],:shape[1]] = melspec
                melspec = padded_array

            # save melspec
            melspec_filename = (os.path.join(folder, file)).replace('.wav', '.mel')
            np.savez(melspec_filename, melspec=melspec, fs=fs)

            # save melspec image
            melspec_image_filename = (os.path.join(folder, file)).replace('.wav', '.png')
            fig = plt.figure(figsize=(10, 4))
            plt.imshow(melspec, origin='lower')
            plt.tight_layout()
            save_image(filepath=melspec_image_filename,fig=fig)
            plt.close()

            # reconstruct audio
            melspec = melspec[:,:-1] #remove original pad
            signal = reconstruct_wav_from_melspec(melspec=melspec, fs=fs, plot=False)
            recon_wav_filename = (os.path.join(folder,'recon', file))
            scipy.io.wavfile.write(recon_wav_filename, fs, signal)


def save_image(filepath, fig=None):
    '''Save the current image with no whitespace
    Example filepath: "myfig.png" or r"C:\myfig.pdf"
    '''
    if not fig:
        fig = plt.gcf()

    plt.subplots_adjust(0,0,1,1,0,0)
    for ax in fig.axes:
        ax.axis('off')
        ax.margins(0,0)
        ax.xaxis.set_major_locator(plt.NullLocator())
        ax.yaxis.set_major_locator(plt.NullLocator())
    fig.savefig(filepath, pad_inches = 0, bbox_inches='tight')


def get_melspec_from_wav(wavfile, n_mels=64, plot=False):
    """
    Given a path to a wav file, returns a melspectrogram array.
    np.ndarray [shape=(n_mels, t)]
    :param wavfile: The input wav file.
    :param n_mels: The number of mel spectrogram filters.
    :param plot: Flag to either plot the spectorgram or not.
    :return: Returns a tuple of np.ndarray [shape=(n_mels, t)] and fs
    """
    sig, fs = librosa.load(wavfile,sr=None)

    # Normalize audio to between -1.0 and +1.0
    sig /= np.max(np.abs(sig), axis=0)

    if len(sig) < fs: # pad if less than a second
        shape = np.shape(sig)
        padded_array = np.zeros(fs)
        padded_array[:shape[0]] = sig
        sig = padded_array

    melspec = librosa.feature.melspectrogram(y=sig,
                                             sr=fs,
                                             center=True,
                                             n_fft=mel_spec_frame_size,
                                             hop_length=int(mel_spec_frame_size/2),
                                             n_mels=n_mels)

    if plot:
        plt.figure(figsize=(8, 6))
        plt.xlabel('Time')
        plt.ylabel('Mel-Frequency')
        librosa.display.specshow(librosa.power_to_db(melspec, ref=np.max),
                                 y_axis='mel',
                                 fmax=fs/2,
                                 sr=fs,
                                 hop_length=int(mel_spec_frame_size / 2),
                                 x_axis='time')
        plt.colorbar(format='%+2.0f dB')
        plt.title('Mel spectrogram')
        plt.tight_layout()
        plt.show()

    return melspec, fs


def reconstruct_wav_from_melspec(melspec, fs, plot=False):
    """
    Given a mel-spectrogram, and a target sampling frequency, reconstructs audio.
    :param melspec: Mel-spectrogram, np.ndarray [shape=(n_mels, t)]
    :param fs: Sampling frequency
    :param plot: Flag to either plot the wav or not.
    :return: The reconstructed raw samples of an audio signal.
    """

    #convert normed image back to dB range
    melspec = melspec * highest_db

    # convert db back to power
    melspec = librosa.db_to_power(melspec)

    sig = librosa.feature.inverse.mel_to_audio(M=melspec,
                                               sr=fs,
                                               center=True,
                                               n_fft=mel_spec_frame_size,
                                               hop_length=int(mel_spec_frame_size/2))

    # Normalize audio to between -1.0 and +1.0
    sig /= np.max(np.abs(sig), axis=0)

    if len(sig) < fs: # pad if less than a second
        shape = np.shape(sig)
        padded_array = np.zeros(fs)
        padded_array[:shape[0]] = sig
        sig = padded_array

    if plot:
        plt.figure(figsize=(10, 4))
        plt.xlabel('Sample')
        plt.ylabel('Amplitude')
        plt.plot(sig)
        plt.title('Waveform')
        plt.tight_layout()
        plt.show()

    return sig


def get_classes_in_datapath(datapath='./data'):
    """
    Returns a list of sub-folders in the data path, assuming each subfolder is a separate class. Each sub-folder
    is associated with a numeric class label, which is also returned.
    :param datapath: Main data path
    :return: Tuple of individual class folder paths, and the corresponding numeric class label for each folder
    """
    subfolders = [f.path for f in os.scandir(datapath) if f.is_dir()]
    class_labels = np.arange(0,len(subfolders))
    return subfolders, class_labels


def get_wav_files_in_path(datapath):
    """
    Returns the list of .wav files in a directory.
    :param datapath: Directory to search for wav files in.
    :return: List of paths to wav files.
    """
    files = os.listdir(datapath)
    files_wav = [i for i in files if i.endswith('.wav')]
    return files_wav


def get_npz_files_in_path(datapath):
    """
    Returns the list of .npz files in a directory.
    :param datapath: Directory to search for npz files in
    :return: List of paths to npz files
    """
    files = os.listdir(datapath)
    files_npz = [i for i in files if i.endswith('.npz')]
    return files_npz

def get_training_set_files():
    """
    Returns a list of training data files (NPZ files)
    :return:
    """
    training_images = []

    subfolders, class_labels = get_classes_in_datapath()

    for folder in subfolders:
        files_mel = get_npz_files_in_path(datapath=folder)
        for file in files_mel:
            training_images.append((os.path.join(folder, file)))

    return training_images
