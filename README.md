## NLP Speech DC-GAN

Daniel Attard - 177900L

## Gitlab repo

Due to size limitations, this zip file does not contain the generated output. Such output can be found at

`https://gitlab.com/daniel2000/ari5121-speech-nlp-2022`
## Files

DCGAN.py contains the GAN model

main.py is a runner program

Preprocessing.py preprocesses the data

plotGraphs.py plots the loss and accraucy curves

modelFigures\ contains the loss and accuracy curves by 3 very similar models

data\ contains the original data

outputs\ contains the generated samples by the trained GAN

## Required Modules

- keras
- numpy
- tensorflow
- scipy 
- librosa
- matplotlib
- pandas