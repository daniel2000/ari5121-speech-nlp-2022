from keras.models import Sequential, Model
from keras.layers.convolutional import Conv2D, UpSampling2D
from keras.layers import LeakyReLU, Input, Dense, Reshape, Flatten, Dropout, Activation, ReLU, BatchNormalization, Conv2DTranspose
from tensorflow.keras.optimizers import Adam
from numpy.random import randn
from numpy.random import choice
import scipy.io.wavfile
import numpy as np
from pathlib import Path
from datetime import datetime
from keras.utils.vis_utils import plot_model

import matplotlib.pyplot as plt
from Preprocessing import reconstruct_wav_from_melspec,save_image


class DCGAN():
    def __init__(self, training_set, epochs, batch_size):
        # Input shape
        self.img_rows = 128
        self.img_cols = 64
        self.channels = 1
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.latent_dim = 32
        self.trainingset = training_set
        self.discriminator_summary = None
        self.generator_summary = None
        self.epochs = epochs
        self.batch_size = batch_size
        self.save_dir = None


        now = datetime.now()
        dt_string = now.strftime("%d.%m.%Y.%H.%M.%S")

        self.save_dir = f"outputs/output_batch_size={self.batch_size}_epochs={self.epochs}_version={dt_string}"

        Path(self.save_dir).mkdir(parents=True, exist_ok=False)

        optimizer = Adam(0.0001, 0.5)

        # Build and compile the discriminator
        self.discriminator = self.build_discriminator()
        self.discriminator.compile(loss='binary_crossentropy',
            optimizer=optimizer,
            metrics=['accuracy'])

        # Build the generator
        self.generator = self.build_generator()

        # The generator takes noise as input and generates imgs
        z = Input(shape=(self.latent_dim,))
        img = self.generator(z)

        # For the combined model we will only train the generator
        self.discriminator.trainable = False

        # The discriminator takes generated images as input and determines validity
        valid = self.discriminator(img)

        # The combined model (stacked generator and discriminator)
        # Trains the generator to fool the discriminator
        self.combined = Model(z, valid)
        self.combined.compile(loss='binary_crossentropy', optimizer=optimizer)

    # generate points in latent space as input for the generator
    def generate_latent_points(self, latent_dim, n_samples):
        noise = np.random.normal(0, 1, (n_samples, self.latent_dim))
        return noise

    def build_generator(self):
        """
        Defines a Generator network.
        :return:
        """

        depth = 64

        model = Sequential(name='Generator')
        model.add(Dense(4*4*int(depth*4),input_shape=(self.latent_dim,)))
        model.add(Reshape((4,4,int(depth*4))))
        model.add(ReLU())

        model.add(Conv2DTranspose(128,kernel_size=5,strides=(2,2),padding='same'))
        model.add(LeakyReLU(alpha=0.2))

        model.add(Conv2DTranspose(128,kernel_size=3,strides=(2,2),padding='same'))
        model.add(LeakyReLU(alpha=0.2))

        model.add(Conv2DTranspose(256, kernel_size=3, strides=(2,2), padding='same'))
        model.add(LeakyReLU(alpha=0.2))

        model.add(Conv2DTranspose(256, kernel_size=3, strides=(2,2), padding='same'))
        model.add(LeakyReLU(alpha=0.2))

        model.add(Conv2D(512, kernel_size=3, strides=1, padding='same'))
        model.add(ReLU())
        model.add(UpSampling2D())
        model.add(Conv2D(1, kernel_size=3, strides=(1,2), padding='same'))

        model.add(Activation('tanh'))

        print(' ================= Generator ================= ')
        model.summary()

        summarylist = []
        model.summary(print_fn=lambda x: summarylist.append(x))
        self.generator_summary = "\n".join(summarylist)

        noise = Input(shape=(self.latent_dim,))
        img = model(noise)

        plot_model(model, to_file=self.save_dir + '/generator.png', show_shapes=True, show_layer_names=True)

        return Model(noise, img)

    def build_discriminator(self):
        """
        Defines a Discriminator network.
        :return:
        """
        depth = 16

        model = Sequential(name='Discriminator')
        model.add(Conv2D(32, kernel_size=3, strides=1, padding='same', input_shape=self.img_shape))
        model.add(ReLU())

        model.add(Conv2D(64, kernel_size=3, strides=2, padding='same'))
        model.add(LeakyReLU(0.2))

        model.add(Conv2D(128, kernel_size=3, strides=2, padding='same'))
        model.add(LeakyReLU(0.2))

        model.add(Conv2D(256, kernel_size=3, strides=2, padding='same'))
        model.add(ReLU())

        model.add(Flatten())
        model.add(Dropout(0.4))
        model.add(Dense(1, activation='sigmoid'))

        print(' ================= Discriminator ================= ')
        model.summary()

        summarylist = []
        model.summary(print_fn=lambda x: summarylist.append(x))
        self.discriminator_summary = "\n".join(summarylist)

        img = Input(shape=self.img_shape)
        validity = model(img)

        plot_model(model, to_file=self.save_dir + '/discriminator.png', show_shapes=True, show_layer_names=True)

        return Model(img, validity)

    def train(self):



        model_output_file = open(self.save_dir + '/modeloutput.txt', 'w')
        model_output_file.write("7 - 5 kernal\n")
        model_output_file.write("Generator\n")
        model_output_file.writelines(self.generator_summary)
        model_output_file.write('\n')
        model_output_file.write("Discriminator\n")
        model_output_file.writelines(self.discriminator_summary)
        model_output_file.write('\n\n')





        half_batch = int(self.batch_size/2)

        # Load the dataset and shuffle it
        X_train = np.asarray(self.trainingset)
        np.take(X_train, np.random.permutation(X_train.shape[0]), axis=0, out=X_train)

        # steps per epoch
        steps_per_epoch = int(X_train.shape[0]/half_batch)

        for epoch in range(self.epochs):
            for step in range(steps_per_epoch):
                # ---------------------
                #  Train Discriminator
                # ---------------------
                # Adversarial ground truths
                # Change to 0.9
                valid = np.ones((half_batch, 1))
                fake = np.zeros((half_batch, 1))

                # Select next batch of images (and shuffle indexes)
                idx = np.arange(step*half_batch,(step*half_batch)+half_batch)
                np.take(idx,np.random.permutation(idx.shape[0]),axis=0,out=idx)
                imgs = []
                for file_path in X_train[idx]:
                    npzfile = np.load(file_path)
                    melspec = npzfile['melspec']
                    imgs.append(melspec)
                imgs = np.asarray(imgs)
                imgs = np.expand_dims(imgs, axis=3)


                # Sample noise and generate a batch of new images
                noise = self.generate_latent_points(self.latent_dim, half_batch)
                gen_imgs = self.generator.predict(noise)

                # Train the discriminator (real classified as ones and generated as zeros)
                d_loss_real, d_acc_real = self.discriminator.train_on_batch(imgs, valid)
                d_loss_fake, d_acc_fake = self.discriminator.train_on_batch(gen_imgs, fake)
                d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
                d_acc = 0.5 * np.add(d_acc_real, d_acc_fake)


                # ---------------------
                #  Train Generator
                # ---------------------

                # Train the generator (wants discriminator to mistake images as real)
                # Sample noise and generate a batch of new images
                noise = self.generate_latent_points(self.latent_dim, self.batch_size)
                valid = np.ones((self.batch_size, 1))
                g_loss = self.combined.train_on_batch(noise, valid)

                # Plot the progress

                output_string = "Epoch: (%d/%d) Step: (%d/%d) [D: loss_R: %f, loss_F: %f, loss: %f, acc_R: %.2f%%, acc_F: %.2f%%, acc.: %.2f%%] [G: loss: %f]" % (epoch, self.epochs-1, step, steps_per_epoch-1, d_loss_real, d_loss_fake, d_loss, d_acc_real*100, d_acc_fake*100, d_acc*100, g_loss)
                print(output_string)
                model_output_file.write(output_string)
                model_output_file.write('\n')

            # Save generated image samples at every epoch
            self.save_imgs(epoch, self.save_dir)

        model_output_file.close()

    def save_imgs(self, epoch, save_dir):
        samples = 10
        noise = self.generate_latent_points(self.latent_dim, samples)
        gen_imgs = self.generator.predict(noise)

        for sample_idx in range(0,samples):
            melspec = gen_imgs[sample_idx, :,:,0]

            fig = plt.figure(figsize=(10, 4))
            plt.imshow(melspec, origin='lower')
            plt.tight_layout()
            save_image(filepath=(save_dir + "/epoch_%d_sample_%d.png" % (epoch,sample_idx)), fig=fig)
            plt.close()

            # reconstruct audio
            melspec = melspec[:,:-1] #remove original pad
            signal = reconstruct_wav_from_melspec(melspec=melspec, fs=16000, plot=False)
            filepath=(save_dir + "/epoch_%d_sample_%d.wav" % (epoch,sample_idx))
            scipy.io.wavfile.write(filepath, 16000, signal)
