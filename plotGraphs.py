import matplotlib.pyplot as plt
import pandas as pd

modelFiles = [
    'outputs\\1output_batch_size=32_epochs=1000_version=29.05.2022.08.09.44\\modelOutput.txt',
    'outputs\\2output_batch_size=64_epochs=1000_version=28.05.2022.19.48.38\\modelOutput.txt',
    'outputs\\3output_batch_size=64_epochs=1000_version=28.05.2022.11.40.45\\modelOutput.txt',

]

masterDFs = []

for modelFile in modelFiles:
    # Obtaining the loss and acc values from the respective files
    df = pd.DataFrame()
    averages = pd.DataFrame()
    masterDf = pd.DataFrame()

    epochs = []
    d_loss = []
    d_acc = []
    g_loss = []

    file = open(modelFile,'r')

    # Obtaining the average of each epoch
    for line in file:
        if 'Epoch: ' in line:
            splits = line.split()

            df2 = pd.DataFrame({'epoch':[int(splits[1][1:-5])],'d_loss':[float(splits[10][:-1])],'d_acc':[float(splits[16][:-2])],'g_loss':[float(splits[19][:-1])]})
            df = pd.concat([df,df2],ignore_index = True)


    for i in range(1000):
        df3 = df[(df.epoch == i)]
        temp = df3.mean()
        if temp.g_loss > 6:
            temp.g_loss = 6
        masterDf = masterDf.append(temp,ignore_index = True)

    masterDFs.append(masterDf)

# Plotting the data
plt.plot(masterDFs[0].epoch,masterDFs[0].d_loss,label='d_loss0')
plt.plot(masterDFs[0].epoch,masterDFs[0].g_loss,label='g_loss0')

plt.plot(masterDFs[1].epoch,masterDFs[1].d_loss,label='d_loss1')
plt.plot(masterDFs[1].epoch,masterDFs[1].g_loss,label='g_loss1')

plt.plot(masterDFs[2].epoch,masterDFs[2].d_loss,label='d_loss2')
plt.plot(masterDFs[2].epoch,masterDFs[2].g_loss,label='g_loss2')

plt.xlabel('Epochs')
plt.legend()

plt.show()
plt.close()

plt.plot(masterDFs[0].epoch, masterDFs[0].d_acc, label='d_acc0')
plt.plot(masterDFs[1].epoch, masterDFs[1].d_acc, label='d_acc1')
plt.plot(masterDFs[2].epoch, masterDFs[2].d_acc, label='d_acc2')

plt.xlabel('Epochs')
plt.legend()

plt.show()






